package dvkotlin.demo.security.repository

import dvkotlin.demo.security.entity.Authority
import dvkotlin.demo.security.entity.AuthorityName
import org.springframework.data.repository.CrudRepository

interface AuthorityRepository: CrudRepository<Authority, Long> {
    fun findByName(input: AuthorityName): Authority
}