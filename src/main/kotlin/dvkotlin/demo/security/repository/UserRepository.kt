package dvkotlin.demo.security.repository

import dvkotlin.demo.security.entity.JwtUser
import org.springframework.data.repository.CrudRepository


interface UserRepository: CrudRepository<JwtUser, Long> {
    fun findByUsername(username: String): JwtUser
}