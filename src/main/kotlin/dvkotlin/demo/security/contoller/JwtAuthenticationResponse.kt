package dvkotlin.demo.security.contoller

data class JwtAuthenticationResponse(
        var token: String? = null
)