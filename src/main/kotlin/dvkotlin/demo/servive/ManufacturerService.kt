package dvkotlin.demo.servive

import dvkotlin.demo.entity.Manufacturer
import dvkotlin.demo.entity.dto.ManufacturerDto

interface ManufacturerService {
    fun save (manu:ManufacturerDto):Manufacturer
    fun getManufacturers(): List<Manufacturer>
}
