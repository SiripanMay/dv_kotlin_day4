package dvkotlin.demo.servive

import dvkotlin.demo.dao.ManufacturerDao
import dvkotlin.demo.entity.Manufacturer
import dvkotlin.demo.entity.dto.ManufacturerDto
import dvkotlin.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ManafacturerServiceImpl:ManufacturerService{
    override fun getManufacturers(): List<Manufacturer> {
        return manufacturerDao.getManufacturers()
    }

    override fun save(manu: ManufacturerDto): Manufacturer {
        val manufacturer=MapperUtil.INSTANCE.mapManufacturer(manu)
        return manufacturerDao.save(manufacturer)
    }

    @Autowired
    lateinit var manufacturerDao: ManufacturerDao


}