package dvkotlin.demo.dao

import dvkotlin.demo.entity.Manufacturer

interface ManufacturerDao{
    //fun getManufacturers() : List<Manufacturer>
    fun save (manufacturer: Manufacturer):Manufacturer

    fun getManufacturers(): List<Manufacturer>
    fun findById(manuId: Long): Manufacturer?
//    fun findById(manuId: Long): List<Manufacturer>
}