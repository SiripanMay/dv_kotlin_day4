package dvkotlin.demo.dao

import dvkotlin.demo.entity.Product
import dvkotlin.demo.entity.SelectedProduct
import org.springframework.data.domain.Page

interface SelectedProductDao{
    fun getSelectedProducts(): List<SelectedProduct>
    fun getSelectedProductByProductName(name:String):List<SelectedProduct>
    fun getSelectedProductByProductNameWithPage(name: String,page:Int,pageSize:Int): Page<SelectedProduct>
//    fun save(selectedProduct: MutableList<SelectedProduct>): List<SelectedProduct>

}