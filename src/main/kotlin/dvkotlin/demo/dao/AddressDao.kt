package dvkotlin.demo.dao

import dvkotlin.demo.entity.Address

interface AddressDao{
    fun getAddresses(): List<Address>
    fun save(add: Address): Address
    fun findById(id: Long): Address?

}