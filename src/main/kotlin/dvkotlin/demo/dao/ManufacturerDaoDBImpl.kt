package dvkotlin.demo.dao

import dvkotlin.demo.entity.Manufacturer
import dvkotlin.demo.repository.ManufacturerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ManufacturerDaoDBImpl:ManufacturerDao{
    override fun findById(manuId: Long): Manufacturer? {
        return manuRepository.findById(manuId).orElse(null)
    }

    override fun getManufacturers(): List<Manufacturer> {
        return manuRepository.findAll().filterIsInstance(Manufacturer::class.java)
    }

    override fun save(manufacturer: Manufacturer): Manufacturer {
        return manuRepository.save(manufacturer)
    }

    @Autowired
    lateinit var manuRepository: ManufacturerRepository

}