package dvkotlin.demo.controller

import dvkotlin.demo.entity.dto.PageShoppingCartDto
import dvkotlin.demo.entity.dto.ShoppingCartDto
import dvkotlin.demo.servive.ShoppingCartService
import dvkotlin.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ShoppingCartController{
    @Autowired
    lateinit var shoppingCartService: ShoppingCartService
    @GetMapping("/shoppingcart")
    fun getShoppingCart(): ResponseEntity<Any> {
        val shoppingCarts = shoppingCartService.getShoppingCarts()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapShoppingCartDto(shoppingCarts))
    }

    @GetMapping("/shoppingcartpage")
    fun getShoppingCartWithPage(@RequestParam("page")page:Int,
                        @RequestParam("pageSize")pageSize:Int): ResponseEntity<Any> {
        val output = shoppingCartService.getShoppingCartWithPage(page,pageSize)
        return ResponseEntity.ok(PageShoppingCartDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                shoppingCarts = MapperUtil.INSTANCE.mapShoppingCartDto(output.content)))
    }

    @GetMapping("/shoppingcart/{productName}")
    fun  getShoppingCartByProductName(@PathVariable("productName")name:String): ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapShoppingCartDto(
                shoppingCartService.getShoppingCartByProductName(name))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/shoppingcartpage/{productName}")
    fun  getShoppingCartByProductWithPage(@PathVariable("productName")name:String,
                                      @RequestParam("page")page:Int,
                                      @RequestParam("pageSize") pageSize:Int): ResponseEntity<Any>{
        val output = shoppingCartService.getShoppingCartByProductNameWithPage(name,page,pageSize)
        return ResponseEntity.ok(PageShoppingCartDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                shoppingCarts = MapperUtil.INSTANCE.mapShoppingCartDto(output.content)))
    }

//    @PostMapping("/shoppingcart/{cusId}")
//    fun addShoppingCart(@RequestBody shoppingCartDto: ShoppingCartDto,
//                        @PathVariable cusId:Long):ResponseEntity<Any>{
//        val output = shoppingCartService.save(cusId,MapperUtil.INSTANCE.mapShoppingCartDto(shoppingCartDto))
//        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
//        outputDto?.let { return ResponseEntity.ok (it) }
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
//    }
}