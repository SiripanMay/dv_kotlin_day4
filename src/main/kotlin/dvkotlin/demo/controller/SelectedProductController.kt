package dvkotlin.demo.controller

import dvkotlin.demo.entity.dto.PageSelectedProductDto
import dvkotlin.demo.servive.SelectedProductService
import dvkotlin.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class SelectedProductController{
    @Autowired
    lateinit var selectedProductService: SelectedProductService
    @GetMapping("/selectedproduct")
    fun getSelectedProducts(): ResponseEntity<Any> {
        val selectedProducts = selectedProductService.getSelectedProducts()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapSelectedProductDto(selectedProducts))
    }

    @GetMapping("/selectedproduct/{productName}")
    fun  getShoppingCartByProductName(@PathVariable("productName")name:String): ResponseEntity<Any>{
        val selectedProducts = selectedProductService.getSelectedProductByProductName(name)
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapSelectedProductDto(selectedProducts))
    }

    @GetMapping("/selectedproductpage/{productName}")
    fun  getSelectedProductByProductNameWithPage(@PathVariable("productName")name:String,
                                      @RequestParam("page")page:Int,
                                      @RequestParam("pageSize") pageSize:Int): ResponseEntity<Any>{
        val output = selectedProductService.getSelectedProductByProductNameWithPage(name,page,pageSize)
        return ResponseEntity.ok(PageSelectedProductDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                selectedProducts = MapperUtil.INSTANCE.mapSelectedProductDto(output.content)))
    }


}