package dvkotlin.demo.controller

import dvkotlin.demo.entity.ShoppingCart
import dvkotlin.demo.entity.UserStatus
import dvkotlin.demo.entity.dto.CustomerDto
import dvkotlin.demo.servive.CustomerService
import dvkotlin.demo.servive.ShoppingCartService
import dvkotlin.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*


@RestController
class CustomerController{
    @Autowired
    lateinit var customerService:CustomerService
    @Autowired
    lateinit var shoppingCartService: ShoppingCartService
    @GetMapping("/customer")
    fun getCustomer(): ResponseEntity<Any>{
        val customers = customerService.getCustomers()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomerDto(customers))
    }

    @GetMapping("/customer/query")
    fun getCustomers(@RequestParam("name")name:String):ResponseEntity<Any>{
        var output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByName(name))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/customer/partialQuery")
    fun getCustomerPartial(@RequestParam("name")name:String,
                          @RequestParam("email",required = false)email:String?):ResponseEntity<Any>{
        val output=MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByPartialNameAndEmail(name,name))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customeraddress")
    fun getCustomerByAddress(@RequestParam("province")province:String):ResponseEntity<Any>{
        val output=MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByAddress(province))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/status")
    fun getCustomerByStatus(@RequestParam("status")status:UserStatus):ResponseEntity<Any>{
        val output=MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByStatus(status))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/product")
    fun getCustomerByProduct(@RequestParam("product")name:String):ResponseEntity<Any>{
        val output=MapperUtil.INSTANCE.mapCustomerProductDto(
                shoppingCartService.getCustomerByProduct(name))
        return ResponseEntity.ok(output)

    }


    @PostMapping("/customer/address/{addressId}")
    fun addCustomerWithOld(@RequestBody customerDto: CustomerDto,@PathVariable addressId:Long):ResponseEntity<Any>{
        val output = customerService.save(addressId,MapperUtil.INSTANCE.mapCustomerDto(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
        outputDto?.let { return ResponseEntity.ok(it)  }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PostMapping("/customer")
    fun addCustomerWithNew(@RequestBody customerDto: CustomerDto):ResponseEntity<Any>{
        val output = customerService.saveWithNew(MapperUtil.INSTANCE.mapCustomerDto(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
        outputDto?.let { return ResponseEntity.ok(it)  }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @DeleteMapping("/customer/{id}")
    fun deleteCustomer(@PathVariable("id")id:Long):ResponseEntity<Any>{
        val customer = customerService.remove(id)
        val outputCustomer = MapperUtil.INSTANCE.mapCustomerDto(customer)
        outputCustomer?.let { return ResponseEntity.ok(it)  }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the product id is not found")
    }

}
