//package dvkotlin.demo.controller
//
//import dvkotlin.demo.entity.Product
//import org.springframework.http.ResponseEntity
//import org.springframework.web.bind.annotation.*
//
//@RestController
//class AssessmentController{
//    @GetMapping("/getAppName")
//    fun getAppName(): String {
//        return "assessment"
//    }
//
//    @GetMapping("/product")
//    fun getProduct(): ResponseEntity<Any>{
//        val product = Product("iPhone","A new telephone",28000,5)
//        return ResponseEntity.ok(product)
//    }
//
//    @GetMapping("/product/{myproduct}")
//    fun getProductWithPath(@PathVariable("myproduct") myproduct:String)
//                           : ResponseEntity<Any> {
//        val product = Product("iPhone","A new telephone",28000,5)
//        if(myproduct.equals(product.name)) {
//            return ResponseEntity.ok(product)
//        }else{
//            return ResponseEntity.notFound().build()
//        }
//    }
//
//    @PostMapping("/setZeroQuantity")
//    fun resetQuantity(@RequestBody product: Product): ResponseEntity<Any>{
//        product.quantity=0
//        return ResponseEntity.ok(product)
//    }
//
//    @PostMapping("/totalPrice")
//    fun getTotalPrice(@RequestBody product: List<Product>): ResponseEntity<Any>{
//        var totalPrice :Int = 0
//        for(i in product){
//            totalPrice += i.price
//        }
//        return ResponseEntity.ok(totalPrice)
//    }
//
//    @PostMapping("avaliableProduct")
//    fun getAvaliableProduct(@RequestBody product: List<Product>): ResponseEntity<Any>{
//        var products = mutableListOf<Product>()
//        for(i in product){
//            if(i.quantity !=0){
//                products.add(i)
//            }
//        }
//        return ResponseEntity.ok(products)
//    }
//
//
//
//}