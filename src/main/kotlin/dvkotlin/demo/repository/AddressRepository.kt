package dvkotlin.demo.repository

import dvkotlin.demo.entity.Address
import org.springframework.data.repository.CrudRepository

interface AddressRepository: CrudRepository<Address, Long>