package dvkotlin.demo.repository

import dvkotlin.demo.entity.ShoppingCart
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository

interface ShoppingCartRepository: CrudRepository<ShoppingCart, Long> {
    fun findBySelectedProduct_Product_NameContainingIgnoreCase(name:String):List<ShoppingCart>
    fun findBySelectedProduct_Product_NameContainingIgnoreCase(name: String,page: Pageable): Page<ShoppingCart>
    fun findAll(page: Pageable): Page<ShoppingCart>
}