package dvkotlin.demo.repository

import dvkotlin.demo.entity.Manufacturer
import org.springframework.data.repository.CrudRepository

interface ManufacturerRepository: CrudRepository<Manufacturer,Long>{
    fun findByName(name:String):Manufacturer
}