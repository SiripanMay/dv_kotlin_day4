package dvkotlin.demo.entity

enum class ShoppingCartStatus {
    WAIT, CONFIRM,PAID,SENT,RECEIVED
}