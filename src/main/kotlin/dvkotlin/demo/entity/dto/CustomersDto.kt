package dvkotlin.demo.entity.dto


data class CustomersDto (
        var name: String?= null,
        var email: String?=null,
        var username: String?=null,
        var authorities: List<AuthorityDto> = mutableListOf()
)