package dvkotlin.demo.entity.dto

data class AddressDto (
        var homeAddress: String?=null,
        var subdistrict: String?=null,
        var district: String?=null,
        var province: String?=null,
        var postcode: String?=null,
        var id:Long? = null
)