package dvkotlin.demo.entity.dto


import dvkotlin.demo.entity.User
import dvkotlin.demo.entity.UserStatus

data class CustomerDto (
        var name: String?= null,
        var email: String?=null,
        var userStatus: UserStatus? = UserStatus.PENDING,
        var defaultAddress : AddressDto?=null,
        var id:Long?=null
)

//data class CustomerDto (
//        var name: String?= null,
//        var email: String?=null,
//        var username: String?=null,
////        var userStatus: UserStatus? = UserStatus.PENDING,
////        var defaultAddress : AddressDto?=null,
////        var id:Long?=null
//        var authorities: List<AuthorityDto> = mutableListOf()
//
//)