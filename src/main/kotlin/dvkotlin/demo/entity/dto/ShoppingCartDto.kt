package dvkotlin.demo.entity.dto

import dvkotlin.demo.entity.ShoppingCartStatus

data class ShoppingCartDto(
        var shoppingCartStatus: ShoppingCartStatus? = ShoppingCartStatus.WAIT,
        var selectedProduct: List<SelectedProductDto>?=null,
        var customer: CustomerDto?=null
)