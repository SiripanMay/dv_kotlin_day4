package dvkotlin.demo.entity.dto

data class ProductDto(var name: String?=null,
                      var description:String?=null,
                      var price:Double?=null,
                      var amountInt: Int?=null,
                      var imageUrl: String?=null,
                      var manu: ManufacturerDto?=null,
                      var id:Long?=null
)